package com.vsu.Project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vsu.Project.dao.UniversityRepository;
import com.vsu.Project.entity.University;

@Service
public class UniversityService {
    @Autowired
    private UniversityRepository universityRepo;

    public List<University> getUniversities() {
        return universityRepo.findAll();
    }

    public University getUniversityById(final Long id) {
        return universityRepo.getOne(id);
    }

    public Long save(final University university) {
        return universityRepo.save(university).getId();
    }

    public void delete(final Long id) {
        universityRepo.deleteById(id);
    }
}
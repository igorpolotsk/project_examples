package com.vsu.Project.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vsu.Project.dao.RoomRepository;
import com.vsu.Project.entity.Room;
import com.vsu.Project.entity.Student;

@Service
public class RoomService {
    @Autowired
    private RoomRepository roomRepo;

    public List<Room> getRooms() {
        return roomRepo.findAll();
    }

    public List<Student> getStudentsByRoomId(final Long id) {
        List<Student> students = new ArrayList<>();
        Room room = roomRepo.findById(id).orElse(null);
        if (room != null && room.getStudents() != null)
            students = room.getStudents();
        return students;
    }

    public Room getRoomById(final Long id) {
        return roomRepo.getOne(id);
    }

    public Long save(final Room room) {
        return roomRepo.save(room).getId();
    }

    public void delete(final Long id) {
        roomRepo.deleteById(id);
    }
}
package com.vsu.Project.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vsu.Project.dao.HostelRepository;
import com.vsu.Project.entity.Hostel;
import com.vsu.Project.entity.Room;

@Service
public class HostelService {
    @Autowired
    private HostelRepository hostelRepo;

    public List<Hostel> getHostels() {
        List<Hostel> hostels = hostelRepo.findAll();
        return hostels;
    }

    public List<Room> getRoomsByHostelId(final Long id) {
        List<Room> rooms = new ArrayList<>();
        Hostel hostel = hostelRepo.findById(id).orElse(null);
        if (hostel != null && hostel.getRooms() != null)
            rooms = hostel.getRooms();
        return rooms;
    }

    public Hostel getHostelById(final Long id) {
        return hostelRepo.getOne(id);
    }

    public Long save(final Hostel hostel) {
        return hostelRepo.save(hostel).getId();
    }

    public void delete(final Long id) {
        hostelRepo.deleteById(id);
    }
}
package com.vsu.Project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vsu.Project.dao.StudentRepository;
import com.vsu.Project.entity.Student;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepo;

    public List<Student> getStudents() {
        return studentRepo.findAll();
    }

    public Student getStudentById(final Long id) {
        return studentRepo.getOne(id);
    }

    public Long save(final Student student) {
        return studentRepo.save(student).getId();
    }

    public void delete(final Long id) {
        studentRepo.deleteById(id);
    }
}
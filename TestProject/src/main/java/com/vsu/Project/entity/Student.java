package com.vsu.Project.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Students")
public class Student extends AbstractEntity {
    @Column(name = "surname", nullable = false)
    protected String surname;

    @Column(name = "name", nullable = false)
    protected String name;

    @Column(name = "patronymic", nullable = false)
    protected String patronymic;

    @Column(name = "passportId", nullable = false)
    protected String passportId;

    @Column(name = "phone", nullable = false)
    protected String phone;

    @ManyToOne
    @JoinColumn(nullable = false, name = "room_id")
    protected Room room;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPassportId() {
        return passportId;
    }

    public void setPassportId(String passportId) {
        this.passportId = passportId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
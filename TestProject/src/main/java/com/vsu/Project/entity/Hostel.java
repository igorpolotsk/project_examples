package com.vsu.Project.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Hostels")
public class Hostel extends AbstractEntity {
    @Column(name = "name", nullable = false)
    protected String name;

    @Column(name = "address", nullable = false)
    protected String address;

    @OneToMany(mappedBy = "hostel")
    @JsonIgnoreProperties("hostel")
    protected List<Room> rooms;

    @ManyToMany
    @JsonIgnoreProperties("hostels")
    protected List<University> universities;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<University> getUniversities() {
        return universities;
    }

    public void setUniversities(List<University> universities) {
        this.universities = universities;
    }
}

package com.vsu.Project.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Universities")
public class University extends AbstractEntity {
    @Column(name = "name", nullable = false)
    protected String name;

    @Column(name = "address", nullable = false)
    protected String address;

    @Column(name = "capacityBild", nullable = false)
    protected Integer capacityBild;

    @ManyToMany(mappedBy = "universities")
    @JsonIgnoreProperties("universities")
    protected List<Hostel> hostels;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCapacityBild() {
        return capacityBild;
    }

    public void setCapacityBild(Integer capacityBild) {
        this.capacityBild = capacityBild;
    }

    public List<Hostel> getHostels() {
        return hostels;
    }

    public void setHostels(List<Hostel> hostels) {
        this.hostels = hostels;
    }
}
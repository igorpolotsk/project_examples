package com.vsu.Project.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Rooms")
public class Room extends AbstractEntity {
    @Column(name = "roomNumber", nullable = false)
    protected Integer roomNumber;

    @Column(name = "capacity", nullable = false)
    protected Integer capacity;

    @OneToMany(mappedBy = "room")
    @JsonIgnoreProperties("room")
    protected List<Student> students;

    @ManyToOne
    @JoinColumn(nullable = false, name = "hostel_id")
    protected Hostel hostel;

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Hostel getHostel() {
        return hostel;
    }

    public void setHostel(Hostel hostel) {
        this.hostel = hostel;
    }
}

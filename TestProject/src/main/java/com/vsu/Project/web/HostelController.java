package com.vsu.Project.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.service.HostelService;
import com.vsu.Project.service.UniversityService;

@Controller
public class HostelController {
    @Autowired
    private HostelService hostelService;
    
    @Autowired
    private UniversityService universityService;

    @RequestMapping(method = RequestMethod.GET, value = "/hostel")
    public String viewHostel(final Model model) {
        model.addAttribute("universities", universityService.getUniversities());
        return "hostel";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/hostel", params = { "id" })
    public String viewHostel(final Model model, @RequestParam final Long id) {
        model.addAttribute("hostel", hostelService.getHostelById(id));
        model.addAttribute("universities", universityService.getUniversities());
        return "hostel";
    }
}
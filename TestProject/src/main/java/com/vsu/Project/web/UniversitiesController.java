package com.vsu.Project.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.entity.University;
import com.vsu.Project.service.UniversityService;

@Controller
public class UniversitiesController {
    @Autowired
    private UniversityService universityService;

    @GetMapping("/universities")
    public String viewUniversities(final Model model) {
        model.addAttribute("universities", universityService.getUniversities());
        return "universities";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/universities", params = { "name", "address",
            "capacityBild" })
    public void save(final Model model, @RequestParam final String name, @RequestParam final String address,
            @RequestParam final Integer capacityBild) {
        University university = new University();
        university.setName(name);
        university.setAddress(address);
        university.setCapacityBild(capacityBild);
        universityService.save(university);

        model.addAttribute("universities", universityService.getUniversities());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/universities", params = { "id", "name", "address",
            "capacityBild" })
    public void save(final Model model, @RequestParam final Long id, @RequestParam final String name,
            @RequestParam final String address, @RequestParam final Integer capacityBild) {
        University university = new University();
        university.setId(id);
        university.setName(name);
        university.setAddress(address);
        university.setCapacityBild(capacityBild);
        universityService.save(university);

        model.addAttribute("universities", universityService.getUniversities());
    }

    @PostMapping("/universities")
    public void save(@RequestBody @Valid final University university) {
        universityService.save(university);
    }

    @DeleteMapping("/universities")
    public void deleteHostels(@RequestParam final Long id) {
        universityService.delete(id);
    }
}
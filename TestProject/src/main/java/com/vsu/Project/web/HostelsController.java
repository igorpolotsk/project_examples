package com.vsu.Project.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.entity.Hostel;
import com.vsu.Project.entity.University;
import com.vsu.Project.service.HostelService;
import com.vsu.Project.service.UniversityService;

@Controller
public class HostelsController {
    @Autowired
    private HostelService hostelService;

    @Autowired
    private UniversityService universityService;

    @GetMapping("/hostels")
    public String viewHostels(final Model model) {
        model.addAttribute("hostels", hostelService.getHostels());
        return "hostels";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/hostels", params = { "name", "address", "universityIds" })
    public void saveHostel(final Model model, @RequestParam final String name, @RequestParam final String address,
            @RequestParam final Long[] universityIds) {
        Hostel hostel = new Hostel();
        hostel.setName(name);
        hostel.setAddress(address);

        List<University> universities = new ArrayList<>();
        for (Long universityId : universityIds)
            universities.add(universityService.getUniversityById(universityId));
        hostel.setUniversities(universities);
        hostelService.save(hostel);

        model.addAttribute("hostels", hostelService.getHostels());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/hostels", params = { "id", "name", "address",
            "universityIds" })
    public void saveHostel(final Model model, @RequestParam final Long id, @RequestParam final String name,
            @RequestParam final String address, @RequestParam final Long[] universityIds) {
        Hostel hostel = new Hostel();
        hostel.setId(id);
        hostel.setName(name);
        hostel.setAddress(address);

        List<University> universities = new ArrayList<>();
        for (Long universityId : universityIds)
            universities.add(universityService.getUniversityById(universityId));
        hostel.setUniversities(universities);
        hostelService.save(hostel);

        model.addAttribute("hostels", hostelService.getHostels());
    }

    @DeleteMapping("/hostels")
    public void deleteHostels(@RequestParam final Long id) {
        hostelService.delete(id);
    }
}
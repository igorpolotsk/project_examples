package com.vsu.Project.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.service.HostelService;
import com.vsu.Project.service.RoomService;

@Controller
public class RoomController {
    @Autowired
    private HostelService hostelService;

    @Autowired
    private RoomService roomService;

    @RequestMapping(method = RequestMethod.GET, value = "/room")
    public String viewRoom(final Model model) {
        model.addAttribute("hostels", hostelService.getHostels());
        return "room";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/room", params = { "id" })
    public String viewRoomById(final Model model, @RequestParam final Long id) {
        model.addAttribute("room", roomService.getRoomById(id));
        model.addAttribute("hostels", hostelService.getHostels());
        return "room";
    }
}
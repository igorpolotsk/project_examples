package com.vsu.Project.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.service.RoomService;
import com.vsu.Project.service.StudentService;

@Controller
public class StudentController {
    @Autowired
    private RoomService roomService;

    @Autowired
    private StudentService studentService;

    @RequestMapping(method = RequestMethod.GET, value = "/student")
    public String viewStudent(final Model model) {
        model.addAttribute("rooms", roomService.getRooms());
        return "student";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/student", params = { "id" })
    public String viewStudentById(final Model model, @RequestParam final Long id) {
        model.addAttribute("student", studentService.getStudentById(id));
        model.addAttribute("rooms", roomService.getRooms());
        return "student";
    }
}
package com.vsu.Project.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.entity.Room;
import com.vsu.Project.entity.Student;
import com.vsu.Project.service.HostelService;
import com.vsu.Project.service.RoomService;

@Controller
public class RoomsController {
    @Autowired
    private HostelService hostelService;

    @Autowired
    private RoomService roomService;

    @GetMapping("/rooms")
    public String viewRooms(final Model model) {
        model.addAttribute("rooms", roomService.getRooms());
        return "rooms";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/rooms", params = { "roomNumber", "capacity", "hostel_id" })
    public void saveRoom(final Model model, @RequestParam final Integer roomNumber,
            @RequestParam final Integer capacity, @RequestParam final Long hostel_id) {
        Room room = new Room();
        room.setRoomNumber(roomNumber);
        room.setCapacity(capacity);
        room.setHostel(hostelService.getHostelById(hostel_id));
        roomService.save(room);

        model.addAttribute("rooms", roomService.getRooms());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/rooms", params = { "id", "roomNumber", "capacity",
            "hostel_id" })
    public void saveRoom(final Model model, @RequestParam final Long id, @RequestParam final Integer roomNumber,
            @RequestParam final Integer capacity, @RequestParam final Long hostel_id) {
        Room room = new Room();
        room.setId(id);
        room.setRoomNumber(roomNumber);
        room.setCapacity(capacity);
        room.setHostel(hostelService.getHostelById(hostel_id));

        List<Student> students = new ArrayList<>();
        for (Student studentId : roomService.getStudentsByRoomId(id)) {
            students.add(studentId);
        }
        room.setStudents(students);
        roomService.save(room);

        model.addAttribute("rooms", roomService.getRooms());
    }

    @DeleteMapping("/rooms")
    public void deleteRooms(@RequestParam final Long id) {
        roomService.delete(id);
    }
}
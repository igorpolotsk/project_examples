package com.vsu.Project.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.entity.Student;
import com.vsu.Project.service.RoomService;
import com.vsu.Project.service.StudentService;

@Controller
public class StudentsController {
    @Autowired
    private RoomService roomService;

    @Autowired
    private StudentService studentService;

    @GetMapping("/students")
    public String viewStudents(final Model model) {
        model.addAttribute("students", studentService.getStudents());
        return "students";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/students", params = { "name", "surname", "patronymic",
            "passportId", "phone", "room_id" })
    public void save(final Model model, @RequestParam final String name, @RequestParam final String surname,
            @RequestParam final String patronymic, @RequestParam final String passportId,
            @RequestParam final String phone, @RequestParam final Long room_id) {
        Student student = new Student();
        student.setName(name);
        student.setSurname(surname);
        student.setPatronymic(patronymic);
        student.setPassportId(passportId);
        student.setPhone(phone);
        student.setRoom(roomService.getRoomById(room_id));
        studentService.save(student);

        model.addAttribute("students", studentService.getStudents());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/students", params = { "id", "name", "surname", "patronymic",
            "passportId", "phone", "room_id" })
    public void save(final Model model, @RequestParam final Long id, @RequestParam final String name,
            @RequestParam final String surname, @RequestParam final String patronymic,
            @RequestParam final String passportId, @RequestParam final String phone, @RequestParam final Long room_id) {
        Student student = new Student();
        student.setId(id);
        student.setName(name);
        student.setSurname(surname);
        student.setPatronymic(patronymic);
        student.setPassportId(passportId);
        student.setPhone(phone);
        student.setRoom(roomService.getRoomById(room_id));
        studentService.save(student);

        model.addAttribute("students", studentService.getStudents());
    }

    @DeleteMapping("/students")
    public void deleteStudents(@RequestParam final Long id) {
        studentService.delete(id);
    }
}
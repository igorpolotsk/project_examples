package com.vsu.Project.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vsu.Project.service.UniversityService;

@Controller
public class UniversityController {
    @Autowired
    private UniversityService universityService;

    @RequestMapping(method = RequestMethod.GET, value = "/university")
    public String viewUniversity() {
        return "university";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/university", params = { "id" })
    public String viewUniversity(final Model model, @RequestParam final Long id) {
        model.addAttribute("university", universityService.getUniversityById(id));
        return "university";
    }
}
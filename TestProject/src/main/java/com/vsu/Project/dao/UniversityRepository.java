package com.vsu.Project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vsu.Project.entity.University;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {

}

package com.vsu.Project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vsu.Project.entity.Hostel;

@Repository
public interface HostelRepository extends JpaRepository<Hostel, Long> {

}
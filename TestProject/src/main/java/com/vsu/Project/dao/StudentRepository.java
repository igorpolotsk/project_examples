package com.vsu.Project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vsu.Project.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

}
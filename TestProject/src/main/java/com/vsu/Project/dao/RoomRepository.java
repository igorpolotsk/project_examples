package com.vsu.Project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vsu.Project.entity.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

}
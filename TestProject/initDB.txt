insert into hostels (id, name, address) values
(1, 'Общежитие №3', 'у.Фрунзе'),
(2, 'Общежитие №4', 'у.Фрунзе'),
(3, 'Общежитие №5', 'у.Фрунзе');

insert into rooms (id, room_number, capacity, hostel_id) values
(1, 513, 4, 1),
(2, 410, 4, 2),
(3, 114, 4, 1);

insert into students (id, surname, name, patronymic, passport_id, phone, room_id) values
(1, 'Стуков', 'Петр', 'Алексеевич', 'BM6464545', 555-1234, 1),
(2, 'Иванов', 'Игорь', 'Андреевич', 'BM345636', 555-4321, 1),
(3, 'Иванов', 'Семен', 'Игоревич', 'BM353474', 555-4567, 2);

insert into universities (id, name, address, capacity_bild) values
(1, 'ВГУ', 'Московкий пр.', 5),
(2, 'ВГМУ', 'у.Фрунзе', 6),
(3, 'ВГТУ', 'Московкий пр.', 4);

insert into university_hostel (university_id, hostel_id) values
(1, 1),
(1, 2),
(2, 3);
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Photer</title>
    <style><%@include file="/WEB-INF/jsp/css/main.css"%></style>
</head>

<body>
    <section>
        <div class="row header header_size-md header_size-lg header_size-sm">
            <div class="col-lg-1 col-md-1 col-sm-1"></div>
            <div class="col-lg-1 col-md-1 col-sm-2 container-right">
                <a href="index.html">
                    <img src="img/logo.png" alt="logo.png" class="header__logo header__logo_size-lg header__logo_size-md header__logo_size-sm">
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-0 container-left">
                <div class="header__header header__header_size-lg header__header_size-md header__header_size-sm">Photer</div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5"></div>
            <div class="col-lg-1 col-md-1 col-sm-1 container-right">
                
            </div>
            <div class="col-lg-1 col-md-1 col-sm-2 container-left">
                <a href="profile.html">
                    <img src="img/profile.png" alt="profile.png"
                        class="header__profile-picture header__profile-picture_size-lg header__profile-picture_size-md header__profile-picture_size-sm">
                </a>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1"></div>
        </div>
    </section>
    <section>
        <div class="container-center">
            <div class="body body_size-lg body_size-md body_size-sm">
                <form action="registration.html">
                    <br>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 container-center">Логин:</div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 container-center">
                            <input class="finder__text-field finder__text-field_size-lg finder__text-field_size-md finder__text-field_size-sm" type="text" id="login" name="login">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 container-center">Пароль:</div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 container-center">
                            <input class="finder__text-field finder__text-field_size-lg finder__text-field_size-md finder__text-field_size-sm" type="text" id="password" name="password">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 container-center">
                            <a class="delete-photo-button delete-photo-button_size-lg delete-photo-button_size-md delete-photo-button_size-sm" href="index.html">Назад</a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 container-center">
                            <input class="add-photo-button add-photo-button_size-lg add-photo-button_size-md add-photo-button_size-sm" type="submit" value="Регистрация">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>

</html>
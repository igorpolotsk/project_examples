<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>

<html>

<head lang="ru">
<meta charset="UTF-8">
<title>Tags</title>
</head>

<body>
    <form action="deleteTag.html">
        <table border="1">
            <tr>
                <td>ID</td>
                <td>name</td>
                <td>deleting</td>
            </tr>
            <c:forEach var="tag" items="${tags}">
                <tr>
                    <td>${tag.id}</td>
                    <td>${tag.name}</td>
                    <td><input type="checkbox"></td>
                </tr>
            </c:forEach>
        </table>
        <a href="add-tag-form.html">Добавить</a>
        <input type="submit" value="Удалить">
    </form>
</body>

</html>
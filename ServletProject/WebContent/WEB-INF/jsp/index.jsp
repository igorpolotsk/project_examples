<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head lang="ru">
<meta charset="UTF-8">
<title>Photer</title>
<style><%@include file="/WEB-INF/jsp/css/main.css"%></style>
</head>

<body>
    <section>
        <div class="row header header_size-md header_size-lg header_size-sm">
            <div class="col-lg-1 col-md-1 col-sm-1"></div>
            <div class="col-lg-1 col-md-1 col-sm-2 container-right">
                <a href="index.html">
                    <img src="img/logo.png" alt="logo.png" class="header__logo header__logo_size-lg header__logo_size-md header__logo_size-sm">
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-0 container-left">
                <div class="header__header header__header_size-lg header__header_size-md header__header_size-sm">Photer</div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5"></div>
            <div class="col-lg-1 col-md-1 col-sm-1 container-right">
                <a class="header__links-button" href="#links">⏷</a>
                <div id="links" class="links links_size-lg links_size-md links_size-sm">
                    <div>
                        <a href="#close" title="Закрыть" class="links__close">x</a>
                        <ul>
                            <c:forEach var="bookmark" items="${bookmarks}">
                                <li>
                                    <a class="links__link" href="openByUser.html?author=${bookmark.idAuthor}">${bookmark.loginAuthor}</a>
                                    <a class="links__link" href="deleteBookmark.html?id=${bookmark.id}">x</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-2 container-left">
                <a href="profile.html">
                    <img src="img/profile.png" alt="profile.png"
                        class="header__profile-picture header__profile-picture_size-lg header__profile-picture_size-md header__profile-picture_size-sm">
                </a>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1"></div>
        </div>
    </section>
    <section>
        <div class="container-center">
            <div class="body body_size-lg body_size-md body_size-sm">
                <br>
                <section>
                    <div class="row tagger tagger_size-all">
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <c:forEach var="tag" items="${tags}">
                                    <div class="swiper-slide">
                                        <a href="openByTag.html?tag=${tag.id}" class="tagger__tag">${tag.name}</a>
                                    </div>
                                </c:forEach>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </section>
                <br>
                <section>
                    <div class="area">
                        <c:forEach var="image" items="${images}">
                            <div class="container">
                                <a class="container__author" href="openByUser.html?author=${image.idAuthor}">${image.loginAuthor}</a>
                                <a href="img/${image.path}">
                                    <img class="container__photo container__photo_size-all lazy" src="img/${image.pathMin}" alt="Photo description">
                                </a>
                                <div class="container-center">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 container-left">
                                            <a href="rate.html?id=${image.idImage}" class="container__like" type="submit">♥${image.rate}</a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 container-right">
                                            <a href="addBookmark.html?id=${image.idAuthor}" class="container__favorite">★</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </section>
            </div>
        </div>
    </section>
    <script type="text/javascript"><%@include file="/WEB-INF/jsp/js/main.js"%></script>
</body>

</html>
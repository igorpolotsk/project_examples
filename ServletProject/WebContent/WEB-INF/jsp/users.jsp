<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>

<html>

<head lang="ru">
<meta charset="UTF-8">
<title>Users</title>
</head>
<body>
    <form action="deleteUser.html">
        <table border="1">
            <tr>
                <td>ID</td>
                <td>login</td>
                <td>deleting</td>
            </tr>
            <c:forEach var="user" items="${users}">
                <tr>
                    <td>user.id</td>
                    <td>user.login</td>
                    <td><input type="checkbox"></td>
                </tr>
            </c:forEach>
        </table>
        <input type="submit" value="Удалить">
    </form>
</body>

</html>
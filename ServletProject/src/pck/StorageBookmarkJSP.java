package pck;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServlet;

public class StorageBookmarkJSP extends HttpServlet {
    private static String jdbcUrl = null;
    private static String jdbcUser = null;
    private static String jdbcPassword = null;

    public static void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword)
            throws ClassNotFoundException {
        Class.forName(jdbcDriver);
        StorageBookmarkJSP.jdbcUrl = jdbcUrl;
        StorageBookmarkJSP.jdbcUser = jdbcUser;
        StorageBookmarkJSP.jdbcPassword = jdbcPassword;
    }

    public static BookmarkJSP readLogin(int idUser) throws SQLException {
        String sql = "SELECT `id`, `login` " + "FROM `users` WHERE `id` = " + idUser;
        Connection c = null;
        Statement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);

            BookmarkJSP object = new BookmarkJSP();
            if (r.next()) {
                object.setIdAuthor(r.getInt("id"));
                object.setLoginAuthor(r.getString("Login"));
            }

            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
    }
}

package pck;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Collection<Tag> tags = StorageTag.readAll();
            req.setAttribute("tags", tags);

            User sessionUser = (User) req.getSession().getAttribute("user");
            if (sessionUser != null) {
                Collection<Bookmark> bookmarks = StorageBookmark.readAll(sessionUser.getId());
                Collection<BookmarkJSP> bookmarksJSP = new ArrayList<>();

                for (Bookmark object : bookmarks) {
                    BookmarkJSP bookmarkJSP = StorageBookmarkJSP.readLogin(object.getIdUser());
                    bookmarkJSP.setId(object.getId());
                    bookmarksJSP.add(bookmarkJSP);
                }
                req.setAttribute("bookmarks", bookmarksJSP);
            }

            Collection<Image> images = StorageImage.readImageByUser(sessionUser.getId());

            Collection<ImageJSP> imagesJSP = new ArrayList<>();

            ImageJSP imageJSP = null;
            for (int i = 0; i < images.size(); i++) {
                imageJSP = new ImageJSP();
                imageJSP.setId(i);
                imageJSP.setIdAuthor(sessionUser.getId());
                imageJSP.setLoginAuthor(sessionUser.getLogin());
                for (Image image : images) {
                    if (image.getIdUser() == sessionUser.getId()) {
                        imageJSP.setIdImage(image.getId());
                        imageJSP.setPath(image.getPath());
                        imageJSP.setPathMin(image.getPathMin());
                        imageJSP.setRate(image.getRate());
                        imagesJSP.add(imageJSP);
                    }
                }
            }
            req.setAttribute("images", imagesJSP);

            getServletContext().getRequestDispatcher("/WEB-INF/jsp/profile.jsp").forward(req, resp);
        } catch (

        SQLException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

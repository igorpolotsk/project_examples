package pck;

import java.util.Date;

public class Image {
    private Integer id;
    private String path;
    private String pathMin;
    private Date date;
    private Integer rate;
    private Integer idUser;
    private Integer idTag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdTag() {
        return idTag;
    }

    public void setIdTag(Integer idTag) {
        this.idTag = idTag;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPathMin() {
        return pathMin;
    }

    public void setPathMin(String pathMin) {
        this.pathMin = pathMin;
    }
}

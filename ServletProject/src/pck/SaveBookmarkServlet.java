package pck;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveBookmarkServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        
        User user = (User) req.getSession().getAttribute("user");
        
        Integer idUser = Integer.parseInt(req.getParameter("id"));
        try {
            Bookmark bookmark = StorageBookmark.read(idUser, user.getId());
            if(bookmark == null) {
                bookmark = new Bookmark();
                bookmark.setIdUser(Integer.parseInt(req.getParameter("id")));
                bookmark.setIdOwner(user.getId());
                StorageBookmark.create(bookmark);
            }
        } catch (SQLException e) {
            throw new ServletException(e);
        }
        resp.sendRedirect(req.getContextPath() + "/index.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

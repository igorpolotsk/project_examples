package pck;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class StorageUser {
    private static String jdbcUrl = null;
    private static String jdbcUser = null;
    private static String jdbcPassword = null;

    public static void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword)
            throws ClassNotFoundException {
        Class.forName(jdbcDriver);
        StorageUser.jdbcUrl = jdbcUrl;
        StorageUser.jdbcUser = jdbcUser;
        StorageUser.jdbcPassword = jdbcPassword;
    }

    public static Collection<User> readAll() throws SQLException {
        String sql = "SELECT `id`, `login`, `password` " + "FROM `users`";
        Connection c = null;
        Statement s = null;
        ResultSet r = null;

        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);
            Collection<User> objects = new ArrayList<>();
            while (r.next()) {
                User object = new User();
                object.setId(r.getInt("id"));
                object.setLogin(r.getString("login"));
                object.setPassword(r.getString("password"));
                objects.add(object);
            }
            return objects;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static User readById(Integer id) throws SQLException {
        String sql = "SELECT `login`, `password`" + "FROM `users` " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;

        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            r = s.executeQuery();
            User object = null;
            if (r.next()) {
                object = new User();
                object.setId(id);
                object.setLogin(r.getString("login"));
                object.setPassword(r.getString("password"));
            }
            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void create(User object) throws SQLException {
        String sql = "INSERT INTO `users` " + "(`login`, `password`) " + "VALUES " + "(?, ?)";
        Connection c = null;
        PreparedStatement s = null;

        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setString(1, object.getLogin());
            s.setString(2, object.getPassword());
            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void update(User object) throws SQLException {
        String sql = "UPDATE `users` SET " + "`login` = ?, `password` = ? " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;

        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setString(1, object.getLogin());
            s.setString(2, object.getPassword());
            s.setInt(3, object.getId());

            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void delete(Integer id) throws SQLException {
        String sql = "DELETE FROM `users` " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;

        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static boolean checkUser(User user) throws SQLException {
        String sql = "SELECT COUNT(*) AS `amount` " + "FROM `users` " + "WHERE `login` = ? AND `password` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;

        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setString(1, user.getLogin());
            s.setString(2, user.getPassword());
            r = s.executeQuery();
            if (r.next()) {
                return r.getInt("amount") == 1;
            }
            return false;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static Integer getUserId(User object) throws SQLException {
        String sql = "SELECT `id` " + "FROM `users` " + " WHERE `login` = ? AND `password` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;

        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setString(1, object.getLogin());
            s.setString(2, object.getPassword());
            r = s.executeQuery();

            if (r.next()) {
                object.setId(r.getInt("id"));
            }
            return object.getId();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
    }
}

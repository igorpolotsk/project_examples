package pck;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveRateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        
        User user = (User) req.getSession().getAttribute("user");

        Image object = new Image();

        try {
            Integer idImage = Integer.parseInt(req.getParameter("id"));
            
            Like like = StorageLike.read(user.getId(), idImage);
            object.setId(idImage);
            if(like == null) {
                object.setRate(StorageImage.readById(object.getId()).getRate() + 1);
                StorageLike.create(new Like(user.getId(), idImage));
            } else {
                object.setRate(StorageImage.readById(object.getId()).getRate() - 1);
                StorageLike.delete(like.getId());
            }
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        try {
            StorageImage.update(object);
        } catch (SQLException e) {
            throw new ServletException(e);
        }
        resp.sendRedirect(req.getContextPath() + "/index.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

package pck;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SaveImageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        Image image = new Image();
        image.setPath(req.getParameter("pathMax"));
        image.setPath(req.getParameter("pathMin"));

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        Date date;
        try {
            date = df.parse(req.getParameter("date"));
        } catch (ParseException e) {
            date = Calendar.getInstance().getTime();
        }
        image.setDate(date);

        image.setRate(Integer.parseInt(req.getParameter("rate")));
        image.setIdUser(Integer.parseInt(req.getParameter("DateFact")));
        image.setIdTag(Integer.parseInt(req.getParameter("Initials")));

        try {
            image.setId(Integer.parseInt(req.getParameter("id")));
        } catch (NumberFormatException e) {
        }
        try {
            if (image.getId() == null) {
                StorageImage.create(image);
            } else {
                StorageImage.update(image);
            }
        } catch (SQLException e) {
            throw new ServletException(e);
        }
        resp.sendRedirect(req.getContextPath() + "/index.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

package pck;

public class Like {
    private Integer id;
    private Integer idUser;
    private Integer idImage;
    
    public Like() {
        id = -1;
        idUser = -1;
        idImage = -1;
    }
    
    public Like(Integer id, Integer idUser, Integer idImage) {
        this.id = id;
        this.idUser = idUser;
        this.idImage = idImage;
    }
    
    public Like(Integer idUser, Integer idImage) {
        id = -1;
        this.idUser = idUser;
        this.idImage = idImage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdImage() {
        return idImage;
    }

    public void setIdImage(Integer idImage) {
        this.idImage = idImage;
    }
}

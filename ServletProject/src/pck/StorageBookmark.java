package pck;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class StorageBookmark {
    private static String jdbcUrl = null;
    private static String jdbcUser = null;
    private static String jdbcPassword = null;

    public static void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword)
            throws ClassNotFoundException {
        Class.forName(jdbcDriver);
        StorageBookmark.jdbcUrl = jdbcUrl;
        StorageBookmark.jdbcUser = jdbcUser;
        StorageBookmark.jdbcPassword = jdbcPassword;
    }

    public static Collection<Bookmark> readAll(int idOwner) throws SQLException {
        String sql = "SELECT `id`, `idUser`, `idOwner` " + "FROM `bookmarks` WHERE `idOwner` = " + idOwner;
        Connection c = null;
        Statement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);

            Collection<Bookmark> objects = new ArrayList<>();
            while (r.next()) {
                Bookmark object = new Bookmark();
                object.setId(r.getInt("id"));
                object.setIdUser(r.getInt("idUser"));
                object.setIdOwner(r.getInt("idOwner"));
                objects.add(object);
            }
            return objects;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static Bookmark readById(Integer id) throws SQLException {
        String sql = "SELECT `idUser`, `idOwner` " + "FROM `bookmarks` " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            r = s.executeQuery();
            Bookmark object = null;
            if (r.next()) {
                object = new Bookmark();
                object.setId(id);
                object.setIdUser(r.getInt("idUser"));
                object.setIdOwner(r.getInt("idOwner"));
            }
            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }
    
    public static Bookmark read(Integer idUser, Integer idOwner) throws SQLException {
        String sql = "SELECT `id` FROM `bookmarks` " + "WHERE `idUser` = ? AND `idOwner` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, idUser);
            s.setInt(2, idOwner);
            r = s.executeQuery();
            Bookmark object = null;
            if (r.next()) {
                object = new Bookmark();
                object.setId(r.getInt("id"));
                object.setIdUser(idUser);
                object.setIdOwner(idOwner);
            }
            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void create(Bookmark object) throws SQLException {
        String sql = "INSERT INTO `bookmarks` " + "(`idUser`, `idOwner`) " + "VALUES " + "(?,?)";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, object.getIdUser());
            s.setInt(2, object.getIdOwner());

            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void delete(Integer id) throws SQLException {
        String sql = "DELETE FROM `bookmarks` " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
    }
}

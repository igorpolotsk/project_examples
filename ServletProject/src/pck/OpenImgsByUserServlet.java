package pck;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OpenImgsByUserServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Collection<User> users = StorageUser.readAll();
            req.setAttribute("users", users);
            Collection<Tag> tags = StorageTag.readAll();
            req.setAttribute("tags", tags);

            Integer id = Integer.parseInt(req.getParameter("author"));

            Collection<Image> images = StorageImage.readImageByUser(id);

            Collection<ImageJSP> imagesJSP = new ArrayList<>();

            ImageJSP imageJSP = null;

            Integer index = 0;
            for (User user : users) {
                imageJSP = new ImageJSP();
                imageJSP.setId(index++);
                imageJSP.setIdAuthor(user.getId());
                imageJSP.setLoginAuthor(user.getLogin());
                for (Image image : images) {
                    if (image.getIdUser() == user.getId()) {
                        imageJSP.setIdImage(image.getId());
                        imageJSP.setPath(image.getPath());
                        imageJSP.setPathMin(image.getPathMin());
                        imageJSP.setRate(image.getRate());
                        imagesJSP.add(imageJSP);
                    }
                }
            }
            req.setAttribute("images", imagesJSP);
        } catch (SQLException e) {
            throw new ServletException(e);
        }
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(req, resp);
    }
}

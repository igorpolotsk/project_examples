package pck;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class StorageImage {
    private static String jdbcUrl = null;
    private static String jdbcUser = null;
    private static String jdbcPassword = null;

    public static void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword)
            throws ClassNotFoundException {
        Class.forName(jdbcDriver);
        StorageImage.jdbcUrl = jdbcUrl;
        StorageImage.jdbcUser = jdbcUser;
        StorageImage.jdbcPassword = jdbcPassword;
    }

    // вывод 12 фотографий на одной странице
    public static Collection<Image> readAll() throws SQLException {// int carrentPage
        /*
         * String sql = "SELECT `id`, `path`, `date`, `rate`, `idUser`, `idTag` " +
         * "FROM `images` LIMIT " + (carrentPage * 12 - 12) + " , 12";
         */
        String sql = "SELECT `id`, `path`, `pathMin`, `date`, `rate`, `idUser`, `idTag` " + "FROM `images` ";
        Connection c = null;
        Statement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);
            Collection<Image> objects = new ArrayList<>();
            while (r.next()) {
                Image object = new Image();
                object.setId(r.getInt("id"));
                object.setPath(r.getString("path"));
                object.setPathMin(r.getString("pathMin"));

                java.sql.Date dateDB = r.getDate("date");
                java.util.Date date = new java.util.Date(dateDB.getTime());
                object.setDate(date);

                object.setRate(r.getInt("rate"));
                object.setIdUser(r.getInt("idUser"));
                object.setIdTag(r.getInt("idTag"));
                objects.add(object);
            }
            return objects;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    // сортировка по дате
    public static Collection<Image> sortByDate() throws SQLException {// int carrentPage
        /*
         * String sql = "SELECT `id`, `path`, `date`, `rate`, `idUser`, `idTag` " +
         * "FROM `images` LIMIT " + (carrentPage * 12 - 12) +
         * " , 12 ORDER BY `date` DESC";
         */
        String sql = "SELECT `id`, `path`, `pathMin`, `date`, `rate`, `idUser`, `idTag` "
                + "FROM `images` ORDER BY `date` DESC";
        Connection c = null;
        Statement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);
            Collection<Image> objects = new ArrayList<>();
            while (r.next()) {
                Image object = new Image();
                object.setId(r.getInt("id"));
                object.setPath(r.getString("path"));
                object.setPathMin(r.getString("pathMin"));

                java.sql.Date dateDB = r.getDate("date");
                java.util.Date date = new java.util.Date(dateDB.getTime());
                object.setDate(date);

                object.setRate(r.getInt("rate"));
                object.setIdUser(r.getInt("idUser"));
                object.setIdTag(r.getInt("idTag"));
                objects.add(object);
            }
            return objects;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    // сортировка по оценкам
    public static Collection<Image> sortByRate() throws SQLException {// int carrentPage
        /*
         * String sql = "SELECT `id`, `path`, `date`, `rate`, `idUser`, `idTag` " +
         * "FROM `images` LIMIT " + (carrentPage * 12 - 12) +
         * " , 12 ORDER BY `rate` DESC ";
         */
        String sql = "SELECT `id`, `path`, `pathMin`, `date`, `rate`, `idUser`, `idTag` "
                + "FROM `images` ORDER BY `rate` DESC ";
        Connection c = null;
        Statement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);
            Collection<Image> objects = new ArrayList<>();
            while (r.next()) {
                Image object = new Image();
                object.setId(r.getInt("id"));

                object.setPath(r.getString("path"));
                object.setPathMin(r.getString("pathMin"));

                java.sql.Date dateDB = r.getDate("date");
                java.util.Date date = new java.util.Date(dateDB.getTime());
                object.setDate(date);

                object.setRate(r.getInt("rate"));
                object.setIdUser(r.getInt("idUser"));
                object.setIdTag(r.getInt("idTag"));
                objects.add(object);
            }
            return objects;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    // получить фотографии пользователя
    public static Collection<Image> readImageByUser(int idUser) throws SQLException {
        String sql = "SELECT `id`, `path`, `pathMin`, `date`, `rate`, `idUser`, `idTag` " + "FROM `images` "
                + "WHERE `idUser`=" + idUser + " ORDER BY `date` DESC";
        Connection c = null;
        Statement s = null;
        ResultSet r = null;

        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);

            Collection<Image> images = new ArrayList<>();
            while (r.next()) {
                Image object = new Image();
                object.setId(r.getInt("id"));
                object.setPath(r.getString("path"));
                object.setPathMin(r.getString("pathMin"));

                java.sql.Date dateDB = r.getDate("date");
                java.util.Date date = new java.util.Date(dateDB.getTime());
                object.setDate(date);

                object.setRate(r.getInt("rate"));
                object.setIdUser(r.getInt("idUser"));
                object.setIdTag(r.getInt("idTag"));
                images.add(object);
            }
            return images;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    // получить фотографию
    public static Image readById(Integer id) throws SQLException {
        String sql = "SELECT `path`, `pathMin`, `date`, `rate`, `idUser`, `idTag` " + "FROM `images` "
                + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            r = s.executeQuery();
            Image object = null;
            if (r.next()) {
                object = new Image();
                object.setId(id);
                object.setPath(r.getString("path"));
                object.setPathMin(r.getString("pathMin"));

                java.sql.Date dateDB = r.getDate("date");
                java.util.Date date = new java.util.Date(dateDB.getTime());
                object.setDate(date);

                object.setRate(r.getInt("rate"));
                object.setIdUser(r.getInt("idUser"));
                object.setIdTag(r.getInt("idTag"));
            }
            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    // добавить новую фотографию
    public static void create(Image object) throws SQLException {
        String sql = "INSERT INTO `images` " + "(`path`, `pathMin`, `date`, `rate`, `idUser`, `idTag`) " + "VALUES "
                + "(?, ?, ?, ?, ?, ?)";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setString(1, object.getPath());
            s.setString(2, object.getPathMin());
            s.setDate(3, new java.sql.Date(object.getDate().getTime()));
            s.setInt(4, object.getRate());
            s.setInt(5, object.getIdUser());
            s.setInt(6, object.getIdTag());
            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void update(Image object) throws SQLException {
        String sql = "UPDATE `images` SET " + "`rate` = ? " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, object.getRate());
            s.setInt(2, object.getId());

            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    // удаление фотографии
    public static void delete(Integer id) throws SQLException {
        String sql = "DELETE FROM `images` " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
    }
}

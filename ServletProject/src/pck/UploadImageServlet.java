package pck;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/upload")
@MultipartConfig
public class UploadImageServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        User user = (User) req.getSession().getAttribute("user");
		
        Integer idTag = Integer.parseInt(req.getParameter("tag"));
		
        Part filePart = req.getPart("file");		
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
		
        InputStream fileContent = filePart.getInputStream();
        byte[] content = new byte[fileContent.available()];
        fileContent.read(content);
		
        File file = new File("Документы/eclipse-workspace/Project/WebContent/img/" + fileName);//не сохраняет в директорию проекта, а такой вот статичный путь - говно собачье
		file.createNewFile();
        
		FileOutputStream stream = new FileOutputStream(file, false);
        stream.write(content);
        
		Image img = new Image();
        img.setDate(Calendar.getInstance().getTime());
        img.setIdTag(idTag);
        img.setIdUser(user.getId());
        img.setPath(fileName);
        img.setPathMin(fileName);//исправить
        img.setRate(0);
		
        try {
            StorageImage.create(img);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		
        stream.close();
        resp.sendRedirect(req.getContextPath() + "/profile.html");
    }
}

package pck;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MainServlet extends HttpServlet {
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static final String URL = "jdbc:mysql://localhost/coursework?" + "useUnicode=true&" + "characterEncoding=UTF-8";
    public static final String USER = "root2";
    public static final String PASSWORD = "";

    @Override
    public void init() throws ServletException {
        try {
            Storage.init(DRIVER, URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
        try {
            StorageUser.init(DRIVER, URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
        try {
            StorageTag.init(DRIVER, URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
        try {
            StorageImage.init(DRIVER, URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
        try {
            StorageBookmark.init(DRIVER, URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
        try {
            StorageBookmarkJSP.init(DRIVER, URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Collection<User> users = StorageUser.readAll();
            req.setAttribute("users", users);
            Collection<Tag> tags = StorageTag.readAll();
            req.setAttribute("tags", tags);

            User sessionUser = (User) req.getSession().getAttribute("user");
            if (sessionUser != null) {
                Collection<Bookmark> bookmarks = StorageBookmark.readAll(sessionUser.getId());

                Collection<BookmarkJSP> bookmarksJSP = new ArrayList<>();

                for (Bookmark object : bookmarks) {
                    BookmarkJSP bookmarkJSP = StorageBookmarkJSP.readLogin(object.getIdUser());
                    bookmarkJSP.setId(object.getId());
                    bookmarksJSP.add(bookmarkJSP);
                }
                req.setAttribute("bookmarks", bookmarksJSP);
            }

            Collection<Image> images = null;
            /*
             * Integer sortParameter = Integer.parseInt(req.getParameter("sort")); if
             * (sortParameter == null || sortParameter == 0) {
             */
            images = StorageImage.sortByDate();
            /*
             * } else { images = StorageImage.sortByRate(); }
             */

            Collection<ImageJSP> imagesJSP = new ArrayList<>();

            ImageJSP imageJSP = null;

            Integer index = 0;
            for (User user : users) {
                imageJSP = new ImageJSP();
                imageJSP.setId(index++);
                imageJSP.setIdAuthor(user.getId());
                imageJSP.setLoginAuthor(user.getLogin());
                for (Image image : images) {
                    if (image.getIdUser() == user.getId()) {
                        imageJSP.setIdImage(image.getId());
                        imageJSP.setPath(image.getPath());
                        imageJSP.setPathMin(image.getPathMin());
                        imageJSP.setRate(image.getRate());
                        imagesJSP.add(imageJSP);
                    }
                }
            }
            req.setAttribute("images", imagesJSP);

            getServletContext().getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(req, resp);
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

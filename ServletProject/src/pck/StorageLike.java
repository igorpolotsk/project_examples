package pck;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StorageLike extends Storage {
    public static Like readById(Integer id) throws SQLException {
        String sql = "SELECT `idUser`, `idImage` FROM `likes` WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            r = s.executeQuery();
            Like object = null;
            if (r.next()) {
                object = new Like();
                object.setId(id);
                object.setIdUser(r.getInt("idUser"));
                object.setIdImage(r.getInt("idImage"));
            }
            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }
    
    public static Like read(Integer idUser, Integer idImage) throws SQLException {
        String sql = "SELECT `id` FROM `likes` WHERE `idUser` = ? AND `idImage` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, idUser);
            s.setInt(2, idImage);
            r = s.executeQuery();
            Like object = null;
            if (r.next()) {
                object = new Like();
                object.setId(r.getInt("id"));
                object.setIdUser(idUser);
                object.setIdImage(idImage);
            }
            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }
    
    public static void create(Like object) throws SQLException {
        String sql = "INSERT INTO `likes` " + "(`idUser`, `idImage`) " + "VALUES " + "(?, ?)";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, object.getIdUser());
            s.setInt(2, object.getIdImage());

            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }
    
    public static void delete(Integer id) throws SQLException {
        String sql = "DELETE FROM `likes` WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }
}

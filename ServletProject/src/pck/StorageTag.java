package pck;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class StorageTag {
    private static String jdbcUrl = null;
    private static String jdbcUser = null;
    private static String jdbcPassword = null;

    public static void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword)
            throws ClassNotFoundException {
        Class.forName(jdbcDriver);
        StorageTag.jdbcUrl = jdbcUrl;
        StorageTag.jdbcUser = jdbcUser;
        StorageTag.jdbcPassword = jdbcPassword;
    }

    public static Collection<Tag> readAll() throws SQLException {
        String sql = "SELECT `id`, `name` " + "FROM `tags`";
        Connection c = null;
        Statement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);
            Collection<Tag> objects = new ArrayList<>();
            while (r.next()) {
                Tag object = new Tag();
                object.setId(r.getInt("id"));
                object.setName(r.getString("name"));
                objects.add(object);
            }
            return objects;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    // получить фотографию по тегу
    public static Collection<Image> readImageByTag(int idTag) throws SQLException {
        String sql = "SELECT `id`, `path`, `pathMin`, `date`, `rate`, `idUser`, `idTag` " + "FROM `images` "
                + "WHERE `idTag`=" + idTag + " ORDER BY `date` DESC";
        Connection c = null;
        Statement s = null;
        ResultSet r = null;

        try {
            c = getConnection();
            s = c.createStatement();
            r = s.executeQuery(sql);

            Collection<Image> images = new ArrayList<>();
            while (r.next()) {
                Image object = new Image();
                object.setId(r.getInt("id"));
                object.setPath(r.getString("path"));
                object.setPathMin(r.getString("pathMin"));

                java.sql.Date dateDB = r.getDate("date");
                java.util.Date date = new java.util.Date(dateDB.getTime());
                object.setDate(date);

                object.setRate(r.getInt("rate"));
                object.setIdUser(r.getInt("idUser"));
                object.setIdTag(r.getInt("idTag"));
                images.add(object);
            }
            return images;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static Tag readById(Integer id) throws SQLException {
        String sql = "SELECT `name` " + "FROM `tags` " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            r = s.executeQuery();
            Tag object = null;
            if (r.next()) {
                object = new Tag();
                object.setId(id);
                object.setName(r.getString("name"));
            }
            return object;
        } finally {
            try {
                r.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void create(Tag object) throws SQLException {
        String sql = "INSERT INTO `tags` " + "(`name`) " + "VALUES " + "(?)";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setString(1, object.getName());

            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    public static void delete(Integer id) throws SQLException {
        String sql = "DELETE FROM `tags` " + "WHERE `id` = ?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = getConnection();
            s = c.prepareStatement(sql);
            s.setInt(1, id);
            s.executeUpdate();
        } finally {
            try {
                s.close();
            } catch (NullPointerException | SQLException e) {
            }
            try {
                c.close();
            } catch (NullPointerException | SQLException e) {
            }
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
    }
}

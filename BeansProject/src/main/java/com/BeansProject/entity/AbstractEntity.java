package com.BeansProject.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }
}

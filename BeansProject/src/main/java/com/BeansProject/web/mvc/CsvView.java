package com.BeansProject.web.mvc;

import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.BeansProject.entity.Bean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class CsvView extends AbstractCsvView {

    @Override
    protected void buildCsvDocument(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        response.setHeader("Content-Disposition", "attachment; filename=\"beans.csv\"");

        @SuppressWarnings("unchecked")
        List<Bean> beans = (List<Bean>) model.get("beans");
        String[] header = { "Name", "Class", "Parent" };
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

        csvWriter.writeHeader(header);

        for (Bean bean : beans) {
            csvWriter.write(bean, header);
        }
        csvWriter.close();

    }
}

package com.BeansProject.web.mvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.BeansProject.entity.Bean;

@RestController
public class BeanJsonController {

    @Autowired
    private ApplicationContext context;

    @GetMapping("/json")
    public @ResponseBody List<Bean> getJson() {
        List<Bean> beans = new ArrayList<>();

        Bean bean = null;
        Class<?> clazz = null;
        for (String beanStr : context.getBeanDefinitionNames()) {
            clazz = context.getBean(beanStr).getClass();
            bean = new Bean();
            bean.setName(beanStr);
            bean.setClazz(clazz.getName());
            bean.setParent(clazz.getSuperclass());
            bean.setInterfaces(clazz.getInterfaces());
            beans.add(bean);
        }
        return beans;
    }
}
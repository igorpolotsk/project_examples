package com.BeansProject.web.mvc;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.BeansProject.entity.Bean;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

@RestController
public class MustacheBeansController {

    @Autowired
    private ApplicationContext context;

    @GetMapping("/")
    public String beanInfo() throws IOException {
        List<Bean> beans = new ArrayList<>();

        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache m = mf.compile("bean.mustache");
        StringWriter writer = new StringWriter();

        Bean bean = null;
        Class<?> clazz = null;
        for (String beanStr : context.getBeanDefinitionNames()) {
            clazz = context.getBean(beanStr).getClass();
            bean = new Bean();
            bean.setName(beanStr);
            bean.setClazz(clazz.getName());
            bean.setParent(clazz.getSuperclass());
            bean.setInterfaces(clazz.getInterfaces());
            bean.setPath(getPath(bean.getClazz()));
            beans.add(bean);
        }
        Map<String, Object> context = new HashMap<>();
        context.put("beans", beans);

        m.execute(writer, context).flush();
        return writer.toString();
    }

    private static String getPath(String name) {
        StringBuffer str = new StringBuffer();

        if (name.contains("autoconfigure") || name.contains("context")) {
            return str.append("https://docs.spring.io/spring-framework/docs/current/javadoc-api/")
                    .append(name.replace('.', '/')).append(".html").toString();
        } else {
            return str.append("https://docs.spring.io/spring-boot/docs/current/api/").append(name.replace('.', '/'))
                    .append(".html").toString();
        }
    }
}

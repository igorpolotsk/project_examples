package com.BeansProject.web.mvc;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import com.BeansProject.entity.Bean;

public class ExcelView extends AbstractXlsView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        response.setHeader("Content-Disposition", "attachment; filename=\"beans.xls\"");

        @SuppressWarnings("unchecked")
        List<Bean> beans = (List<Bean>) model.get("beans");

        CellStyle style = workbook.createCellStyle();

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);

        Sheet sheet = workbook.createSheet("Beans");

        sheet.setDefaultColumnWidth(60);

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Name");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Class");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Parent");
        header.getCell(2).setCellStyle(style);

        int index = 1;
        for (Bean bean : beans) {
            Row row = sheet.createRow(index++);
            row.createCell(0).setCellValue(bean.getName());
            row.createCell(1).setCellValue(bean.getClazz());
            row.createCell(2).setCellValue(bean.getParent().getName());
        }
        workbook.close();
    }

}

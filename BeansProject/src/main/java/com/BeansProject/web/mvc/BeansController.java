package com.BeansProject.web.mvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.BeansProject.conf.TypeConfig;
import com.BeansProject.entity.Bean;

@Controller
public class BeansController {

    @Autowired
    private ApplicationContext context;

    // thymeleaf
    @GetMapping("/beans.{type}")
    public String beanInfo(@PathVariable String type, final Model model) {
        List<Bean> beans = new ArrayList<>();

        Bean bean = null;
        Class<?> clazz = null;
        for (String beanStr : context.getBeanDefinitionNames()) {
            clazz = context.getBean(beanStr).getClass();
            bean = new Bean();
            bean.setName(beanStr);
            bean.setClazz(clazz.getName());
            bean.setParent(clazz.getSuperclass());
            bean.setInterfaces(clazz.getInterfaces());
            bean.setPath(getPath(bean.getClazz()));
            beans.add(bean);
        }
        TypeConfig.TYPE = type;
        model.addAttribute("beans", beans);
        return "index";
    }

    private static String getPath(String name) {
        StringBuffer str = new StringBuffer();

        if (name.contains("autoconfigure") || name.contains("context")) {
            return str.append("https://docs.spring.io/spring-framework/docs/current/javadoc-api/")
                    .append(name.replace('.', '/')).append(".html").toString();
        } else {
            return str.append("https://docs.spring.io/spring-boot/docs/current/api/").append(name.replace('.', '/'))
                    .append(".html").toString();
        }
    }
}

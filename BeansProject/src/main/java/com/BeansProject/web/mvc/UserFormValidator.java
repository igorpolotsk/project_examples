package com.BeansProject.web.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.BeansProject.entity.User;
import com.BeansProject.service.UserService;

@Component
public class UserFormValidator implements Validator {

    @Autowired
    private UserService service;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.username", "error.user.username.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.password", "error.user.password.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.password.empty");

        final Object value = errors.getFieldValue("password");

        if (value != null && StringUtils.hasText(value.toString())
                && !value.toString().equals(errors.getFieldValue("user.password"))) {
            errors.rejectValue("password", "error.password.confirm");
        }

        if (!errors.hasErrors()) {
            final String username = (String) errors.getFieldValue("user.username");
            final User user = service.getUserByUsername(username);

            if (user != null) {
                errors.rejectValue("user.username", "error.user.exist", new Object[] { user.getUsername() },
                        "User exist!");
            }
        }
    }
}

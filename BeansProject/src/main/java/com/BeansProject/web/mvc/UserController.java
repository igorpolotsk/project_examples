package com.BeansProject.web.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.BeansProject.entity.User;
import com.BeansProject.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private UserFormValidator validator;

    @ModelAttribute("form")
    public UserForm populateUserForm() {
        return new UserForm();
    }

    @GetMapping
    public String edit(final Authentication auth, @ModelAttribute("form") final UserForm form, final Model model) {
        if (auth != null) {
            final User user = userService.getUserByUsername(auth.getName());
            form.setUser(user);
            model.addAttribute("form", form);
        }
        return "user";
    }

    @PostMapping
    public String save(final Authentication auth, final HttpServletRequest request,
            @ModelAttribute("form") final UserForm form, final BindingResult result) {
        validator.validate(form, result);
        if (result.hasErrors())
            return "user";

        final User user = form.getUser();

        userService.saveUser(user);

        if (auth == null) {
            final UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    user.getUsername(), user.getPassword());
            authToken.setDetails(new WebAuthenticationDetails(request));
            final Authentication authentication = authenticationManager.authenticate(authToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        return "redirect:/user";
    }
}

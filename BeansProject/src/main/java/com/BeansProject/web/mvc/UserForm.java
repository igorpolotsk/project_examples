package com.BeansProject.web.mvc;

import java.io.Serializable;

import com.BeansProject.entity.User;

public class UserForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String password;

    private User user;

    public UserForm() {
    }

    public UserForm(User user) {
        setUser(user);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }
}

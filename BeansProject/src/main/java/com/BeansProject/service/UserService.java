package com.BeansProject.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.BeansProject.dao.RoleRepository;
import com.BeansProject.dao.UserRepository;
import com.BeansProject.entity.Role;
import com.BeansProject.entity.User;
import com.BeansProject.util.MD5;

@Service
public class UserService {

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private UserRepository userRepo;

    public Role getRole(final String name) {
        for (Role role : roleRepo.findAll()) {
            if (role.getName().equals(name)) {
                return roleRepo.findById(role.getId()).orElse(new Role());
            }
        }
        return null;
    }

    public User getUserById(final String id) {
        return userRepo.findById(id).orElse(new User());
    }

    public User getUserByUsername(final String username) {
        for (User user : userRepo.findAll()) {
            if (user.getUsername().equals(username)) {
                return userRepo.findById(user.getId()).orElse(new User());
            }
        }
        return null;
    }

    public List<User> getUsers() {
        return userRepo.findAll();
    }

    public void saveUser(final User user) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(MD5.md5(user.getPassword()));

        final Role role = getRole("ROLE_USER");
        newUser.setRoles(Arrays.asList(role));

        newUser.setActive(true);

        userRepo.save(newUser);
    }

    public void deleteUser(final String id) {
        User user = userRepo.findById(id).orElse(new User());
        user.setActive(false);
        userRepo.save(user);
    }
}
package com.BeansProject.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.BeansProject.entity.Role;

public interface RoleRepository extends MongoRepository<Role, String> {

}
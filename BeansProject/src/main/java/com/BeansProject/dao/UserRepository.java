package com.BeansProject.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.BeansProject.entity.User;

public interface UserRepository extends MongoRepository<User, String> {

}

package com.example.TestScheduleProject;

import org.springframework.stereotype.Component;

@Component
public class RunnableTask implements Runnable {

    @Override
    public void run() {
        System.out.println("Hello world " + Thread.currentThread().getName());
    }
}
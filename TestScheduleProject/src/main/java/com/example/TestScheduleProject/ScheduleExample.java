package com.example.TestScheduleProject;

import java.util.concurrent.ScheduledFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.HttpStatus;

@Controller
public class ScheduleExample {
    private CronTrigger cronTrigger = new CronTrigger("* * * * * ?");

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;

    private ScheduledFuture<?> scheduledFuture;

    @RequestMapping("start")
    ResponseEntity<Void> start() {
        scheduledFuture = taskScheduler.schedule(new RunnableTask(), cronTrigger);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping("stop")
    ResponseEntity<Void> stop() {
        if (scheduledFuture != null)
            scheduledFuture.cancel(false);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
